// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "AssetUtilities.generated.h"

/**
 * 
 */
UCLASS()
class INFINITYFALL_API UAssetUtilities : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

		UFUNCTION(BlueprintCallable, Category = "Cloning")
		static AActor* Clone(AActor* source, FTransform spawnLocation);
};
