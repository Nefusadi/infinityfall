// Fill out your copyright notice in the Description page of Project Settings.
#include "AssetUtilities.h"
#include "Runtime/Engine/Classes/Engine/World.h"


AActor* UAssetUtilities::Clone(AActor* source, FTransform spawnLocation)
{
	FActorSpawnParameters Parameters;

	Parameters.Template = source;
	Parameters.Owner = source->GetOwner();
	Parameters.bNoFail = true;
	Parameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	return source->GetWorld()->SpawnActor(source->GetClass(), &spawnLocation, Parameters);
}