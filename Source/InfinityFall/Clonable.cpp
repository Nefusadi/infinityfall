// Fill out your copyright notice in the Description page of Project Settings.

#include "Clonable.h"
#include "Runtime/Engine/Classes/Engine/World.h"

// Sets default values
AClonable::AClonable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AClonable::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AClonable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

AClonable* AClonable::Clone(FTransform spawnLocation)
{
	FActorSpawnParameters Parameters;

	Parameters.Template = this;
	Parameters.Owner = GetOwner();
	Parameters.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	return GetWorld()->SpawnActor<AClonable>(GetClass(), spawnLocation, Parameters);
}

